[[_TOC_]]

## :checkered_flag: Get Started/Prerequisites
- [ ] Ensure the Vulnerability Report contains vulnerabilities as follows:
  - On the left sidebar, select `Secure > Vulnerability report` to view the full report
  - You should see vulnerabilities related to injection errors (1 critical and 2 medium) especially one with `A01:2017 - Injection` as identifier.

## :beetle: Analyze vulnerabilities

### 1\. Understand the security posture of your application

- [ ] Show the overview of the vulnerabilities of your project
  - On the left sidebar, select `Secure > Vulnerability report` to view the full report
  - You will discover vulnerabilities related to injection errors (1 critical and 2 medium).
  - Select the critical vulnerability with `A01:2017 - Injection` as identifier found in `FeatureResource.java`. 
  - The description is very minimal and the provided solution is not relevant in our context.
- [ ] Get more insight about the critical vulnerability, how it could be exploited, and how to fix it.
  - At the bottom of the vulnerability’s page, click the `Explain vulnerability` button.
  - A popup will open on the right-hand side and you will get an explanation on what a SQL injection risk is and why our application is vulnerable using [GitLab's Explain This Vulnerability](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#explaining-a-vulnerability) functionality.
  - Check out the **_Fixed Code Example_** section.

    > TL;DR; If they are curious what triggered this response, try clicking **_Show prompt_** to see the full prompt sent to GitLab Duo to generate the suggested fix.

### 2\. Understand the context of the vulnerability

- [ ] What if we wanted more context about the specific function above before we go and make a code change?  
  - Let's click the linked file in the **_Location_** section to be redirected to our `FeatureResource.java` file.
  - Once in the `FeatureResource.java` file locate the line the command injection vulnerability on line 33 and highlight the whole `getByName` function.
  - You should then see a small question mark (`?`) to the left of the code, click it.
  - On the right hand side there will now be a small pop up called `Code Explanation` which explains what your highlighted code does in natural language.
  - Repeat these steps on specific code sections.
  - At this point we are fully aware of why and how the injection vulnerability is occurring.
- [ ] Let's confirm this vulnerability by changing its status
  - At the top of the vulnerability’s page, find the `Status: Needs triage` field.
  - Click `Needs triage` to expand the options
  - Select `Confirm`. The status will be updated.

## :pill: Implement the fix

### 1\. Create the issue

Now that we have a clean understanding of the vulnerability, we will start the process of remediation.

* [ ] Let's create the issue to plan your work.
  * At the bottom of the injection vulnerability’s page, click the `Create Issue` button.
  * You will be redirected to a form called "New Issue" which will be populated with information of the vulnerability.
  * In the body of the form, you will see a GitLab logo. Click `GitLab logo > Generate issue description`
  * Enter the following description

    ```
    Provide a description with Gherkin syntax on the impact of CWE-89.
    Provide the response in markdown format with headers.
    ```
* [ ] Finalize the issue
  * Uncheck _This issue is confidential and should only be visible to team members with at least Reporter access._
  * Assign the issue to yourself and click the `Create issue` button

### 2\. Remediate the vulnerability

- [ ] First, create the MergeRequest
  - From the newest issue you created, click the `Create merge request` button.
    > If you do find the button, it is probably before your issue is confidential. If so, make it not confidential and Ask GitLab Duo Chat is needed.
  - Uncheck the `Mark as draft` box
  - Assign to yourself and leave all other settings as is
  - Scroll to the bottom then click the `Create merge request` button
- [ ] Fix the SQL Injection vulnerability
  - Open the GitLab WebIDE from the Merge Request
    - In the Merge Request, click the `Code` button in the top right
    - Select `Open in Web IDE`
    - It will open a new tab with the Web IDE and will point directly to the right branch
  - Open the `FeatureResource.java` file
    - Press `Control`+`p` or `Command`+`p` 
    - Type `FeatureResource.java`
    - Select `FeatureResource.java src/main/java/org/acme/resource`
  - Highlight the whole `getByName` method
  - Ask GitLab Duo Chat to explain this method
    ```plaintext
    /explain
    ```
  - Ask GitLab Duo Chat to provide one way to fix the issue with explanations about the key changes that help to prevent SQL injection vulnerabilities
    ```plaintext
    Fix SQL injection vulnerability
    ```

    <details>
    <summary>Click here to see one expected solution</summary>

    ```java
        @GET
        @Path("/getByName")
        @Produces(MediaType.APPLICATION_JSON)
        public Feature getByName(@QueryParam("name") String featureName) {
            Feature result = null;
            Query query = em.createQuery("SELECT f FROM Feature f WHERE f.name = :name", Feature.class);

            query.setParameter("name", featureName);
            result = (Feature) query.getSingleResult();

            return result;
        }
    ```

    </details>

### 3\. Commit your changes

- [ ] Now let's push our changes to the branch. 
  - Select `Source Control` in the `Activity Bar`
  - Write a commit message. Next click `Commit to '<my branch>'`.
  - A popup will appear in the bottom right corner, *Success! Your changes have been committed.*
  - Click the `Go to MR` button. 
- [ ] The vulnerability is now resolved
  - Once the pipeline passed, in the merge request, you will find the following message
    ```
    ✅ Security scanning detected no new potential vulnerabilities
    ```
  - Expand this section and see the vulnerability remediated
    ```
    ✅ SAST detected no new potential vulnerabilities
       Fixed
       🛑 Critical Semgrep Finding: java.lang.security.audit.formatted-sql-string.formatted-sql-string
    ```

## 🧪 Create the unit tests

### 1\. Create the unit test class

Time to run our favorite tasks in software development, unit tests!

- [ ] Generate the unit tests for the FeatureResource class. 
  - In your IDE, open the `src/main/java/org/acme/resource/FeatureResource.java`
  - Highlight the `getByName` method
  - First, ask GitLab Duo Chat :robot: to generate the unit tests
    ```plaintext
    /tests using Quarkus and RestAssured with associated imports
    ```
- [ ] It will generate a `FeatureResourceTest` class with test functions for the method.

    <details>
    <summary>Click here to see one expected solution</summary>

    ```java
    package org.acme.resource;

    import io.quarkus.test.junit.QuarkusTest;
    import org.junit.jupiter.api.Test;

    import static io.restassured.RestAssured.given;
    import static org.hamcrest.CoreMatchers.is;

    @QuarkusTest
    public class FeatureResourceTest {

        @Test
        public void testGetByName() {
            given()
              .when().get("/api/feature/getByName?name=Feature1")
              .then()
                .statusCode(200)
                .body("name", is("Feature1"));
        }
        
        @Test
        public void testGetByNameNotFound() {
            given()
              .when().get("/api/feature/getByName?name=InvalidFeature")
              .then()
                .statusCode(404);
        }

    }
    ```

    </details>
- [ ] Apply the changes
  - Navigate and open `src/main/test/org/acme/resource/FeatureResourceTest.java`
  - Replace the existing content with the AI-generated test class
- [ ] Now let's push our changes to the branch. 
  - Select `Source Control` in the `Activity Bar`
  - Write a commit message. Next click `Commit to '<my branch>'`.
  - A popup will appear in the bottom right corner, *Success! Your changes have been committed.*
  - Click the `Go to MR` button. 
  - 
### 2\. Fix the broken pipeline

- [ ] The pipeline might be now broken ❌ because one of the `java` CI jobs (or both) failed due to multiple reasons
- [ ] Analyze the root cause of the problem
  - In GitLab UI, from the Merge Request, open the `latest pipeline run > one failed job`
  - On the top bar menu, click the `Root cause analysis` button. A right menu will open.
  - Click `Generate root case analysis`. GitLab Duo will analyse your logs to provide a potential way to fix the problem

    <details>
    <summary>Click here to see one expected analysis for java:unittests failed</summary>

    The job failed because the getByName method in the FeatureResource class is throwing a NoResultException when trying to retrieve a Feature entity from the database using the name "InvalidFeature". This is because there is no Feature entity with that name in the database.

    To fix this, you should either add a Feature entity with the name "InvalidFeature" to the database, or modify the getByName method to handle the case where no Feature entity is found.

    *Example Fix:*

    ```java
    @Override
    public Feature getByName(String name) {
        try {
            return featureRepository.findByName(name);
        } catch (NoResultException e) {
            return null;
        }
    }
    ```

    </details>

### 3\. Align your test case with the dataset
- [ ] Copy/Paste the following content for `FeatureResourceTest.java` (or similar)
    ```java
    package org.acme.resource;

    import io.quarkus.test.junit.QuarkusTest;
    import org.junit.jupiter.api.Test;

    import static io.restassured.RestAssured.given;
    import static org.hamcrest.CoreMatchers.is;

    @QuarkusTest
    public class FeatureResourceTest {

        @Test
        public void testGetByName() {
            given()
            .when().get("/api/feature/getByName?name=Code Suggestions")
            .then()
                .statusCode(200)
                .body("name", is("Code Suggestions"));
        }
        
        @Test
        public void testGetByNameNotFound() {
            given()
            .when().get("/api/feature/getByName?name=InvalidFeature")
            .then()
                .statusCode(404);
        }

    }
    ``` 
    > Please copy/paste this content to ensure an alignment with the next steps of the workshop
- [ ] For `testGetByNameNotFound` method, we need to catch the exception when the feature doesn't exist
  - In your IDE, open the `src/main/java/org/acme/resource/FeatureResource.java` file
  - Remove the 2 last lines of the `getByName()` method
    ```java
    featureFound = (Feature) query.getSingleResult();
    return featureFound;
    ```
  - Type the following comment and press `Enter`:
    ```java
    // If no result, throw 404 exception
    ```
  - Automatically, GitLab Duo will suggest a snippet to answer the requirement
  - Press `tab` multiple times to accept the suggestion and adapt the outcome to match as follows (or similar):
    ```java
        @GET
        @Path("/getByName")
        @Produces(MediaType.APPLICATION_JSON)
        public Feature getByName(@QueryParam("name") String featureName) {
            Feature result = null;
        
            Query query = em.createQuery("SELECT f FROM Feature f WHERE f.name = :name", Feature.class);
            query.setParameter("name", featureName);

            // If no result, throw 404 exception
            if (query.getResultList().isEmpty()) {
                throw new NotFoundException();
            }

            result = (Feature) query.getSingleResult();
            return result;
        }
    ```
- [ ] Now let's push our changes to the branch. 
  - Select `Source Control` in the `Activity Bar`
  - Write a commit message. Next click `Commit to '<my branch>'`.
  - A popup will appear in the bottom right corner, *Success! Your changes have been committed.*
  - Click the `Go to MR` button. 
-  [ ] 🌐 The pipeline and the unit tests are now green ✅ 

## :art: Implement new features

### 1\. Develop using Code Generation

- [ ] Create a delete feature API
  - Notice that the `FeatureResource` class does not have a delete method.
  - On a new line within the class, type the following prompt and press `Enter`:
    ```java
    // Delete a feature by name
    ```
  - Code Suggestions will generate the function for you. 
  - Press `tab` to accept the suggestion. Ensure that the generated code matches with the following (otherhwise, make appropriate changes):
    ```java
        @Path("/delete")
        @Transactional
        @Produces(MediaType.APPLICATION_JSON)
        public String delete(@QueryParam("name") String name) {
            Feature feature = Feature.find("name", name).firstResult();
            if(feature == null) {
                throw new NotFoundException();
            }
            feature.delete();
            return "Feature deleted";
        }    
    ```
- [ ] Add additional checks in `add()` method
  - Within the `add()` method use the prompt below to check if the feature already exists and press `Enter`
    ```java
    // check if the feature already exists
    ```
  - Code Suggestions will generate again the function for you. 
  - Press `tab` to accept the suggestion.
  - Repeat the instructions above with the following prompts
    - ```java
      // check if name is alphanumeric
      ```
    - ```java
      // same for description
      ```
    - ```java
      // the version should follow the standard semantic rules only with major and minor release
      ```
### 2\. Develop using Code Completion

- [ ] Create the update resource
  - On a new line within the `FeatureResource` class, `start typing` the following prompt then when a suggestion appears, press `tab` then `Enter` as many times as necessary to complete the function:
    ```java
        @GET
        @Transactional
        @Path("/update")
        @Produces(MediaType.APPLICATION_JSON)
        public String update(@QueryParam("name") String name, @QueryParam("newName") String newName, @QueryParam("newDescription") String newDescription, @QueryParam("newStatus") String newStatus) {

            Feature feature = Feature.find("name", name).firstResult();
            if(feature == null) {
                throw new NotFoundException();
            }

            if(newName != null) {
               feature.setName(newName); 
            }

            if(newDescription != null) {
                feature.setDescription(newDescription);
            }

            if(newStatus != null) {
                feature.setStatus(newStatus);
            }
        
            feature.persist();
            return "Feature updated";
        }
    ```
  - How many characters have you typed before pressing `tab` then `Enter`? Share your answer in the comment of this issue.

    > Please note that Code Suggestions uses your current files as reference for how to write the function, so you may need to do some slight editing for the final result

    > If it ever gets stuck, keep coding :nerd:!

### 3\. Refactor your code

- [ ] Make your code more consistant and readable
  - In your IDE, highlight the `listAll` method from the `FeatureResource` class.
  - Ask GitLab Duo Chat :robot: with the following prompt:
    ```
    /refactor and document
    ```
  - The answer is just amazing 🤩, right?
  - Copy the new code by clicking the clipboard button in the code section and replace the current version.
  - Highlight the `getByName` method and repeat the process above
  - Highlight another method (your choice) and repeat the refactor process ...  
- [ ] After this "hard" coding section, we want to push our changes to the branch. 
  - Select `Source Control` in the `Activity Bar`
  - Write a commit message. Next click `Commit to '<my branch>'`.
  - A popup will appear in the bottom right corner, *Success! Your changes have been committed.*
  - Click the `Go to MR` button.

## :microscope: Review the code changes

### 1\. Summarize the code changes

- [ ] Merge request summaries can be added to your merge request description when creating or editing a merge request
  - In the top-right of the Merge Request, click on `Edit`
  - Then click on `Summarize code changes`.
  - The generated summary is added to the merge request description where your cursor is.
  - Scroll down and click on `Save changes` 

### 2\. Merge the merge request

- [ ] Push the fix in your main branch
  - Select the `Edit commit message` checkbox on the merge widget.
  - Select `Create AI-generated commit message`.
  - Review the commit message provide and click `Insert` to add it to the commit.
  - Then click the `Merge` button.
  - The issue of the SQL injection vulnerability will be closed.
