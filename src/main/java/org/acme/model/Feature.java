// Create a PanacheEntity class with a name, a description and a status
package org.acme.model;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.Entity;

@Entity
public class Feature extends PanacheEntity {

    /** 
     * The name of the feature. 
     */ 
    public String name;
    
    /**
     * A description providing more details about the feature.
     */
    public String description;

    /**
     * The status of the feature e.g. Proposed, In Progress, Done.
     */
    public String status;

    /**
     * The version of the product this feature applies to.
     */
    public String version;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getVersion() {
        return version;
    }
    public void setVersion(String version) {
        this.version = version;
    }
}