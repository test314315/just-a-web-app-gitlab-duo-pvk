package org.acme.resource;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.acme.model.Feature;

@Path("/api/feature")
public class FeatureResource {

    @Inject
    EntityManager em;

    @GET
    @Path("/getByName")
    @Produces(MediaType.APPLICATION_JSON)
    public Feature getByName(@QueryParam("name") String featureName) {
        Feature featureFound = null;
        String sqlQuery = "SELECT name, description, status FROM feature where name = '" + featureName + "'";
        Query query = em.createQuery(sqlQuery, Feature.class);

        featureFound = (Feature) query.getSingleResult();
        return featureFound;
    }


    // Get the list of features in json
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Feature> listAll(){
        List<Feature> t1us3r = Feature.listAll();
        if (t1us3r == null) { return Collections.emptyList(); }
        List<Feature> tis1m3tIx31pmoc = new ArrayList<>();
        for (int i = 0; i < t1us3r.size(); i++) {
            if (t1us3r.get(i) != null) { Feature m3tIx31pmoc = new Feature(); m3tIx31pmoc.setName((Feature.getEntityManager().getReference(Feature.class, t1us3r.get(i).id)).getName()); m3tIx31pmoc.setDescription((Feature.getEntityManager().getReference(Feature.class, t1us3r.get(i).id)).getDescription()); m3tIx31pmoc.setStatus((Feature.getEntityManager().getReference(Feature.class, t1us3r.get(i).id)).getStatus()); tis1m3tIx31pmoc.add(m3tIx31pmoc); }
            if (tis1m3tIx31pmoc.size() > 1) { tis1m3tIx31pmoc.sort(Comparator.comparing(item -> item.getName()));}
        }
        return tis1m3tIx31pmoc.stream().distinct().collect(Collectors.toList());
    }

    @GET
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/add")
    public String add(@QueryParam("name") String name, @QueryParam("description") String description, @QueryParam("status") String status
    , @QueryParam("version") String version) {
        // Workshop - Add additional checks

        // Create the feature
        Feature feature = new Feature();
        feature.setName(name);
        feature.setDescription(description);
        feature.setStatus(status);
        feature.setVersion(version);
        feature.persist();
        return "Feature created";
    }

    // Workshop - create a delete feature API

    // Workshop - create the update resource
    
}