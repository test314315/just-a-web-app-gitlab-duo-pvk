package org.acme.resource;

import java.io.InputStream;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/files")
public class FileResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String listFiles(String dir) throws Exception {
        Runtime rt = Runtime.getRuntime();
        Process proc = rt.exec(new String[] {"sh", "-c", "ls " + dir});
        int result = proc.waitFor();
        if (result != 0) {
          System.out.println("process error: " + result);
        }
        
        InputStream in = 
            (result == 0) ? proc.getInputStream() : proc.getErrorStream();
        
        StringBuilder stringBuilder = new StringBuilder();
        int c;
        while ((c = in.read()) != -1) {
            stringBuilder.append((char) c);
        }

        return stringBuilder.toString();
      }
}
