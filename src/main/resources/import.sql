INSERT INTO feature (ID, name, description, status) 
VALUES
  (1, 'Suggested Reviewers', 'Assists in creating faster and higher-quality reviews by automatically suggesting reviewers for your merge request.', 'GA'),
  (2, 'Code Suggestions', 'Helps you write code more efficiently by viewing code suggestions as you type.', 'BETA'),
  (3, 'Vulnerability summary', 'Helps you remediate vulnerabilities more efficiently, boost your skills, and write more secure code.', 'BETA'),
  (4, 'Code explanation', 'Helps you understand code by explaining it in English language.', 'Experiment'),
  (5, 'GitLab Duo Chat', 'Process and generate text and code in a conversational manner. Helps you quickly identify useful information in large volumes of text in issues, epics, code, and GitLab documentation.', 'BETA'),
  (6, 'Value stream forecasting', 'Assists you with predicting productivity metrics and identifying anomalies across your software development lifecycle.', 'Experiment'),
  (7, 'Discussion summary', 'Assists with quickly getting everyone up to speed on lengthy conversations to help ensure you are all on the same page.', 'Experiment'),
  (8, 'Merge request summary', 'Efficiently communicate the impact of your merge request changes.', 'Experiment'),
  (9, 'Code review summary', 'Helps ease merge request handoff between authors and reviewers and help reviewers efficiently understand suggestions.', 'Experiment'),
  (10, 'Merge request template population', 'Generate a description for the merge request based on the contents of the template.', 'Experiment'),
  (11, 'Test generation', 'Automates repetitive tasks and helps catch bugs early.', 'Experiment'),
  (12, 'Git suggestions', 'Helps you discover or recall Git commands when and where you need them.', 'Experiment'),
  (13, 'Root cause analysis', 'Assists you in determining the root cause for a pipeline failure and failed CI/CD build', 'Experiment'),
  (14, 'Issue description generation', 'Generate issue descriptions.', 'Experiment');
