# Just a Web App - GitLab Duo Mode

Welcome to the GitLab Duo Mode of "Just a Web App". This template is designed to help you run demos consistently and efficiently. Whether you're showcasing GitLab Duo, this template provides a structured framework to ensure a smooth and successful demo every time.

## Getting Started (Once)

To get started with using this project as template, follow these steps:

1. **Create a `GitLab Duo Demo` Group** in your GitLab instance/namespace
2. **Create a subgroup called `Templates`**
3. **Import the project `Just a Web App - GitLab Duo`** under the `GitLab Duo Demo/Templates` group
  - Go to `Templates` group
  - Click on `create new project > Import project`
  - Select `Repository By URL`
  - Fill in the following details
    - `Git repository URL` = `https://gitlab.com/gitlab-learn-labs/webinars/ai/just-a-web-app-gitlab-duo.git`
  - Click on `create project`
4. **Create the templates instructions**
  - Go to the recently created project `Just A Web App Gitlab Duo`
  - Go to `Plan > Issues > New issue`
  - In the Description, select the template `GitLab Duo Walkthrough`
  - Specify `🦊 GitLab Duo Demo Walkthrough 🦊` as Title
  - Click on `Create Issue`
5. **Define this template at the group level**
  - Go to `GitLab Duo Demo` group
  - Go to `Settings > General`
  - Expand the `Custom project templates` section
  - Select the `Templates` Group
  - Click on `Save changes`
  
Once you have set up your `GitLab Duo Demo` Group  and configured it according to your requirements,  you will be ready to spin up a demo project at a glance wherever you will need it

## Running a Demo (For each session)

### Create your session project

1. In the `GitLab Duo Demo` group, click on `New Project > Create from template`.
2. Select the `Group` tab and click the `Use template` for `Just A Web App GitLab Duo`
3. Fill in the project name
4. Click `Create project`
5. Run a pipeline to populate the Vulnerability Report
  - Go to the recently created project
  - Go to `Build > Pipelines`
  - Click on `Run pipeline` on the top right
  - Click on `Run pipeline` again
  - The pipeline will run during about 2 minutes.

### Get the instructions

1. Go to the demo project 
2. Go to `Plan > Issues > 🦊 GitLab Duo Demo Walkthrough 🦊`
3. Follow the guide and make your customer/prospect happy 🥳

## Contact

For any questions/comments, please reach out to (@madou)

